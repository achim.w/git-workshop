#!/usr/bin/env python3
import sys


def factorial(n):
	""" Calculate the factorial for input variable n."""
	if n < 0:
		print('factorial is only defined for positive integer.')
		return None

	f = 1
	for i in range(n):
		f = f * i

	return f


def main():
	# "user_input" is the number for which the factorial will be calculated
	if len(sys.argv) > 1:
		user_input = int(sys.argv[1])
	else:
		user_input = int(input('Please enter a number for calculation of factorial: '))
	
	f = factorial(user_input)
	print(f'Factorial for n={user_input} is {f}.')


main()
